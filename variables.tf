// Main
variable "module_namespace" {
  default = "terraform-aws-acm-cert-expire-monitor"
}

variable "acm_certificate_arn" {
  default = ""
}

// Lambda
variable "lambda_zip_arcive_dir" {
  default = ".terraform/"
}

// CloudWatch
variable "certificate_expire_event_rule_schedule_expression" {
  default = "cron(0 0/12 ? * 1-7 *)"
}

variable "certificate_expire_metric_name" {
  default = "ECM-certificate-days-to-expire"
}

variable "certificate_exprire_monitoring_period" {
  default = 600
}

variable "certificate_exprire_threshold" {
  default = 31
}

variable "certificate_exprire_evaluation_periods" {
  default = 2
}