// Lambda
resource "aws_function" "certificate_monitor" {
  function_name    = "${var.module_namespace}-ACM-certificate-monitor"
  role             = aws_iam_role.certificate_monitor_lambda_iam_role.arn
  handler          = "certificate_monitor.init"
  filename         = "${var.lambda_zip_arcive_dir}/${var.module_namespace}-lambda-ACM-certificate-monitor.zip"
  source_code_hash = filebase64sha256("${var.lambda_zip_arcive_dir}/${var.module_namespace}-lambda-ACM-certificate-monitor.zip")
  runtime          = "python3.6"
  timeout          = 180
  environment {
    variables = {
      CERTIFICATE_ARN      = var.acm_certificate_arn
      CLOUDWATCH_NAMESPACE = var.module_namespace
      CLOUDWATCH_METRIC    = var.certificate_expire_metric_name
    }
  }
  depends_on = [
    data.archive_file.certificate_monitor_lambda,
    aws_iam_role.certificate_monitor_lambda_iam_role
  ]
}

// IAM
resource "aws_iam_role" "certificate_monitor_lambda_iam_role" {
  name = "${var.module_namespace}_certificate_monitor_lambda_iam_role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "certificate_monitor_lambda_iam_role_policy" {
  name = "${var.module_namespace}_certificate_monitor_lambda_iam_role_policy"
  role = aws_iam_role.certificate_monitor_lambda_iam_role.id
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "cloudwatch:*",
        "acm:*"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
EOF
}

// CloudWatch Event
resource "aws_cloudwatch_event_rule" "cerificate_monitor" {
  name                = "${var.module_namespace}-SSL_certificate_monitor_event"
  description         = "Schedule to run Lambda function for ACM certificate monitoring in ${var.module_namespace} environment"
  schedule_expression = var.certificate_expire_event_rule_schedule_expression
  depends_on          = [
    aws_function.certificate_monitor
  ]
}

resource "aws_cloudwatch_event_target" "cerificate_monitor" {
  target_id  = "${var.module_namespace}_certificate_monitor"
  arn        = aws_function.certificate_monitor.arn
  rule       = aws_cloudwatch_event_rule.cerificate_monitor.name
  depends_on = [
    aws_function.certificate_monitor
  ]
}

resource "aws_permission" "cerificate_monitor" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_function.certificate_monitor.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.cerificate_monitor.arn
  depends_on    = [
    aws_function.certificate_monitor
  ]
}

// CloudWatch SSL Expriration Alert
resource "aws_cloudwatch_metric_alarm" "cloudwatch_ssl_expriration_alert" {
  alarm_name          = "${var.module_namespace}-SSL_expriration_alert"
  alarm_description   = "Days to expiration of SSL Certificate in ${var.module_namespace} environment"
  namespace           = var.module_namespace
  metric_name         = var.certificate_expire_metric_name
  extended_statistic  = "p1"
  period              = var.certificate_exprire_monitoring_period
  comparison_operator = "LessThanThreshold"
  threshold           = var.cloudwatch_ssl_expriration_threshold
  evaluation_periods  = var.certificate_exprire_evaluation_periods
  depends_on          = [
    aws_function.certificate_monitor
  ]
}
