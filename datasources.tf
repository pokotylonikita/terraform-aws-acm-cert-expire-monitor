data "archive_file" "certificate_monitor_lambda" {
  type                    = "zip"
  source_content          = "${file("${path.module}/certificate_monitor.py")}"
  source_content_filename = "certificate_monitor.py"
  output_path             = "${var.lambda_zip_arcive_dir}/${var.module_namespace}-lambda-ACM-certificate-monitor.zip"
}
