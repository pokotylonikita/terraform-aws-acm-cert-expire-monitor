
import boto3
import os
from datetime import datetime, timezone


def lambda_init(event, context):
    
    boto3_acm = boto3.client('acm')
    boto3_cloudwatch = boto3.client('cloudwatch')

    certificate_arn = os.environ['CERTIFICATE_ARN']
    metric_namespace = os.environ['CLOUDWATCH_NAMESPACE']
    metric_name = os.environ['CLOUDWATCH_METRIC']
    
    certificate_parameters = boto3_acm.describe_certificate(
        CertificateArn=certificate_arn
    )    
    boto3_cloudwatch.put_metric_data( 
        MetricData = [
            {
                'MetricName': metric_name,
                'Dimensions': [],
                'Unit': 'None',
                'Value': (
                    certificate_parameters['Certificate']['NotAfter'] - datetime.now(timezone.utc)
                ).days
            }
        ],
        Namespace = metric_namespace
    )
